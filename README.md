## Table of contents

- [Table of contents](#table-of-contents)
- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot

Website Mobile View  
![Mobile View](./screenshots/website_mobile_view.jpg)

Website Desktop View  
![Desktop View](./screenshots/website_desktop_view.png)

### Links

- Solution URL: [Project Repo](https://gitlab.com/varshneyankit/3-column-preview-card)
- Live Site URL: [Live Site](https://3-column-preview-card-woad.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Desktop-first workflow

### What I learned

I have used color variables in CSS while working on this project.  
This helped me in maintaining the CSS code.

```css
:root {
  --bright-orange: hsl(31, 77%, 52%);
  --dark-cyan: hsl(184, 100%, 22%);
  --very-dark-cyan: hsl(179, 100%, 13%);
  --transparent-white: hsla(0, 0%, 100%, 0.75);
  --very-light-gray: hsl(0, 0%, 95%);
}
```

### Continued development

I would like to focus on making responsive web designs which are having animating contents.

### Useful resources

- [Web Dev Simplified](https://www.youtube.com/watch?v=1PnVor36_40&list=PLZlA0Gpn_vH9D0J0Mtp6lIiD_8046k3si)
